pushd $PSScriptRoot
Remove-Item -Recurse TestResults
pushd ..
#dotnet test -s Default.runsettings --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=$pwd/CodeCoverage/TestResults/coverage
dotnet test -s Default.runsettings --no-restore
popd
dotnet build CodeCoverage.csproj /t:Coverage
&"C:\Program Files\internet explorer\iexplore.exe" "$pwd\report\index.htm"
popd