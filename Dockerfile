FROM mcr.microsoft.com/dotnet/core/runtime:3.0-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.0-buster AS build
WORKDIR /src
COPY ["src/Miqm.Battleships/Miqm.Battleships.csproj", "src/Miqm.Battleships/"]
RUN dotnet restore "src/Miqm.Battleships/Miqm.Battleships.csproj"
COPY . .
WORKDIR "/src/src/Miqm.Battleships"
RUN dotnet build "Miqm.Battleships.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Miqm.Battleships.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Miqm.Battleships.dll"]