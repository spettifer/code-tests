using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Miqm.Battleships.Handlers;
using Miqm.Battleships.Providers;
using Miqm.Battleships.Services;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Services
{
    public class TestBattleshipsConsoleGameService
    {
        private const string TEST_COMMAND = "testCommand";
        private const string TEST_RESPONSE = "Test Response";
        private readonly BattleshipsConsoleGameService _subject;
        private readonly TextReader _consoleIn;
        private readonly TextWriter _consoleOut;
        private readonly TextWriter _consoleErr;
        private readonly IProvideConsoleStreams _consoleAccessProvider;
        private readonly IHandleConsoleInput _consoleInputHandler;
        private readonly IProvideWelcomeMessage _welcomeMessageProvider;
        private readonly CancellationTokenSource _tokenSource;

        public TestBattleshipsConsoleGameService()
        {
            _consoleIn = Substitute.For<TextReader>();
            _consoleOut = Substitute.For<TextWriter>();
            _consoleErr = Substitute.For<TextWriter>();
            _consoleAccessProvider = Substitute.For<IProvideConsoleStreams>();
            _consoleAccessProvider.ConsoleIn.Returns(_consoleIn);
            _consoleAccessProvider.ConsoleOut.Returns(_consoleOut);
            _consoleAccessProvider.ConsoleErr.Returns(_consoleErr);
            _consoleInputHandler = Substitute.For<IHandleConsoleInput>();
            _welcomeMessageProvider = Substitute.For<IProvideWelcomeMessage>();
            _tokenSource = new CancellationTokenSource();
            _subject = new BattleshipsConsoleGameService(_consoleAccessProvider, _consoleInputHandler, _welcomeMessageProvider);
            SetupSendTestCommand();
        }

        private void SetupSendTestCommand(int commandCount = 1)
        {
            var cmdLeft = commandCount;
            _consoleIn.ReadLineAsync().Returns(async c =>
            {
                if (cmdLeft > 0)
                {
                    await Task.Delay(200);
                    --cmdLeft;
                    return TEST_COMMAND;
                }
                if (cmdLeft == 0)
                {
                    --cmdLeft;
                    _tokenSource.Cancel();
                    await Task.Delay(200);
                }
                return null;
            });
        }
        private async Task RunSubjectAsync()
        {
            await _subject.StartAsync(_tokenSource.Token);
            while (!_tokenSource.Token.IsCancellationRequested) await Task.Delay(100);
            await _subject.StopAsync(CancellationToken.None);
        }

        [Fact]
        internal async Task ShouldReadConsoleInputAndPassToConsoleInputHandler()
        {
            await RunSubjectAsync();
            _consoleInputHandler.Received(1).HandleConsoleInput(Arg.Is(TEST_COMMAND));
        }

        [Fact]
        internal async Task ShouldWriteOutputFromConsoleInputHandlerToConsole_WhenHandlerResponseIsNotNullOrEmpty()
        {
            _consoleInputHandler.HandleConsoleInput(Arg.Any<string>()).Returns(TEST_RESPONSE);
            await RunSubjectAsync();
            _consoleOut.Received(1).WriteLine(Arg.Is<string>(c => c.StartsWith("#") && c.Contains(TEST_RESPONSE)));
        }
        [Fact]
        internal async Task ShouldNotWriteOutputFromConsoleInputHandlerToConsole_WhenHandlerResponseIsNullOrEmpty()
        {
            _consoleInputHandler.HandleConsoleInput(Arg.Any<string>()).Returns(string.Empty);
            await RunSubjectAsync();
            _consoleOut.Received(0).WriteLine(Arg.Is<string>(c => c.StartsWith("#")));
        }

        [Fact]
        internal async Task ShouldClearConsoleOnStart()
        {
            SetupSendTestCommand(5);
            await RunSubjectAsync();
            _consoleAccessProvider.Received(1).Clear();
        }
        [Fact]
        internal async Task ShouldWriteWelcomeMessageOnceAtStartup()
        {
            _welcomeMessageProvider.WelcomeMessage.Returns("WELCOME MESSAGE");
            SetupSendTestCommand(5);
            await RunSubjectAsync();
            _consoleOut.Received(1).WriteLine(Arg.Is<string>(c => string.Equals(c, "WELCOME MESSAGE")));
        }
        [Fact]
        internal async Task ShouldShowPromptWhenWaitingForInput()
        {
            SetupSendTestCommand(5);
            await RunSubjectAsync();
            _consoleOut.Received(6).Write(Arg.Is<string>(c => string.Equals(c, "$> ")));
        }
        [Fact]
        internal async Task ShouldWriteExceptionMessageOnErrorConsole_WhenExceptionCaught()
        {
            SetupSendTestCommand(1);
            const string exceptionMesssage = "Test Exception";
            _consoleInputHandler.HandleConsoleInput(Arg.Any<string>()).Returns(c =>
            {
                throw new Exception(exceptionMesssage);
            });
            await RunSubjectAsync();
            _consoleErr.Received(1).WriteLine(Arg.Is<string>(c => c.Contains(exceptionMesssage)));
        }
    }
}
