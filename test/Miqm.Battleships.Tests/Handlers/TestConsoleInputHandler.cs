using System.Collections.Generic;
using FluentAssertions;
using FluentAssertions.Execution;
using Miqm.Battleships.Handlers.ConsoleCommands;
using Miqm.Battleships.Handlers.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers
{
    public class TestConsoleInputHandler
    {
        private readonly ConsoleInputHandler _subject;
        private readonly IHandleUnknownCommand _unknownCommandHanlder;
        private readonly List<IHandleCommand> _commandHandlers;

        public TestConsoleInputHandler()
        {
            _unknownCommandHanlder = Substitute.For<IHandleUnknownCommand>();
            _commandHandlers = new List<IHandleCommand>() { Substitute.For<IHandleCommand>(), Substitute.For<IHandleCommand>(), Substitute.For<IHandleCommand>() };
            for (var i = 0; i < _commandHandlers.Count; i++)
            {
                _commandHandlers[i].Order.Returns(i + 1);
            }
            _subject = new ConsoleInputHandler(_commandHandlers, _unknownCommandHanlder);
        }

        [Fact]
        internal void HandleConsoleInput_ShouldCheckHandlersAccordingToTheirOrder()
        {
            _commandHandlers[0].Order.Returns(3);
            _commandHandlers[1].Order.Returns(1);
            _commandHandlers[2].Order.Returns(8);
            var callOrder = new List<int>();
            _commandHandlers[0].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c =>
            {
                callOrder.Add(_commandHandlers[0].Order); return false;
            });
            _commandHandlers[1].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c =>
            {
                callOrder.Add(_commandHandlers[1].Order); return false;
            });
            _commandHandlers[2].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c =>
            {
                callOrder.Add(_commandHandlers[2].Order); return false;
            });
            var testInput = "testInput";
            _subject.HandleConsoleInput(testInput);
            callOrder.Should().BeInAscendingOrder(because: $"{nameof(IHandleCommand)} should be called according to {nameof(IHandleCommand.Order)} property.");
        }
        [Theory]
        [InlineData(new[] { false, false, false }, 1)]
        [InlineData(new[] { false, false, true }, 0)]
        [InlineData(new[] { false, true, false }, 0)]
        [InlineData(new[] { true, false, false }, 0)]

        internal void HandleConsoleInput_ShouldUseUnknownCommandHandler_OnlyWhenOtherHandlersReturnedFalse(bool[] handlerOutput, int unknownHandlerExpectedCalls)
        {
            for (var i = 0; i < _commandHandlers.Count; i++)
            {
                var currHanlderOutput = handlerOutput[i];
                _commandHandlers[i].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c =>
                {
                    return currHanlderOutput;
                });
            }
            var testInput = "testInput";
            _subject.HandleConsoleInput(testInput);
            _unknownCommandHanlder.Received(unknownHandlerExpectedCalls).HandleUnknownCommand(Arg.Any<string>());
        }

        [Theory]
        [InlineData(new[] { false, false, false }, new[] { 1, 1, 1 })]
        [InlineData(new[] { false, false, true }, new[] { 1, 1, 1 })]
        [InlineData(new[] { false, true, false }, new[] { 1, 1, 0 })]
        [InlineData(new[] { true, false, false }, new[] { 1, 0, 0 })]
        internal void HandleConsoleInput_ShouldStopExecutingHanndlers_AfterFirstHandlerRespondsWithTrue(bool[] handlerOutput, int[] expectedCalls)
        {
            for (var i = 0; i < _commandHandlers.Count; i++)
            {
                var currHanlderOutput = handlerOutput[i];
                _commandHandlers[i].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c =>
                {
                    return currHanlderOutput;
                });
            }
            var testInput = "testInput";
            _subject.HandleConsoleInput(testInput);
            using (new AssertionScope())
            {
                for (var i = 0; i < _commandHandlers.Count; i++)
                {
                    _commandHandlers[i].Received(expectedCalls[i]).HandleCommand(Arg.Any<string>(), out Arg.Any<string?>());
                }
            }
        }
        [Theory]
        [InlineData("   AAAA   ", "aaaa")]
        [InlineData("   AAAA", "aaaa")]
        [InlineData("aAaA   ", "aaaa")]
        [InlineData("   AA aa   ", "aa aa")]
        [InlineData("\t\taa AA   \r\n", "aa aa")]
        [InlineData("aA\r\n", "aa")]
        internal void HandleConsoleInput_ShouldPassTrimmedLowercaseCommandToHandlers(string testInput, string expectedStringOnHandler)
        {
            _commandHandlers[0].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(true);
            _subject.HandleConsoleInput(testInput);
            _commandHandlers[0].Received().HandleCommand(Arg.Is<string>(expectedStringOnHandler), out Arg.Any<string?>());
        }

        [Fact]
        internal void HandleConsoleInput_ReturnsEmptyString_WhenHandlerReturnsNull()
        {
            _commandHandlers[0].HandleCommand(Arg.Any<string>(), out Arg.Any<string?>()).Returns(c => { c[1] = null; return true; });
            var testInput = "testInput";
            var result = _subject.HandleConsoleInput(testInput);
            result.Should().BeEmpty();
        }
    }
}
