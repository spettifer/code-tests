using System;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Execution;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TestGamePrintCommandHandler
    {
        private const string EXPECTED_COMMAND = "print";
        private const char EXPPECTED_MISS_MARK = '.';
        private const char EXPECTED_SHIP_MARK = '%';
        private const char EXPECTED_HIT_MARK = '#';
        private readonly IGameController _gameController;
        private readonly GamePrintCommandHandler _subject;

        public TestGamePrintCommandHandler()
        {
            _gameController = Substitute.For<IGameController>();
            _subject = new GamePrintCommandHandler(_gameController);
        }


        [Fact]
        internal void Handle_ShouldReturnFalseIfCommandIsNotExpected()
        {
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand("draw", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                response.Should().BeNull();
                result.Should().NotBeNull().And.BeFalse();
            }
        }

        [Fact]
        internal void Handle_ShouldGetBoardFromGameControllerAndReturnTrueAndDrawBoardInResponse_WhenCommandIsExpected()
        {
            _gameController.GetBoard().Returns(c =>
            {
                var boardState = new EGameBoardState[TestProgram.BOARD_SIZE, TestProgram.BOARD_SIZE];
                return boardState;
            });
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand(EXPECTED_COMMAND, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                if (response != null)
                {
                    var lines = response.Split(Environment.NewLine);
                    lines.Should().HaveCount(TestProgram.BOARD_SIZE + 2 + 1 + 1 + 1, because: "Drawn board should have 2 line header, line on bottom and additional line break to start below response sign, and last line should be empty");
                    lines[0].Should().NotBeNullOrWhiteSpace("Fist line should have board description");
                    var lineCount = 0;
                    foreach (var line in lines.Skip(1).Take(TestProgram.BOARD_SIZE + 3))
                    {
                        lineCount++;
                        line.Should().HaveLength(TestProgram.BOARD_SIZE + 3 + 1, because: $"line {lineCount} should have row header delimited from board state and trailing vertical line");
                    }
                    lines[^1].Should().BeEmpty();
                }
            }
            _gameController.Received(1).GetBoard();
        }
        [Theory]
        [InlineData(EGameBoardState.Hit, EXPECTED_HIT_MARK)]
        [InlineData(EGameBoardState.Miss, EXPPECTED_MISS_MARK)]
        [InlineData(EGameBoardState.ShipRemaining, EXPECTED_SHIP_MARK)]
        internal void Handle_ShouldReturnResponseWithMarks_WhenTheyAreReceivedFromGameController(EGameBoardState gameBoardState, char expectedMark)
        {

            _gameController.GetBoard().Returns(c =>
            {
                var boardState = new EGameBoardState[TestProgram.BOARD_SIZE, TestProgram.BOARD_SIZE];
                var r = new Random();
                for (int i = 0; i < 10; i++)
                {
                    int x, y = 0;
                    do
                    {
                        x = r.Next(0, TestProgram.BOARD_SIZE);
                        y = r.Next(0, TestProgram.BOARD_SIZE);
                    } while (boardState[x, y] != EGameBoardState.Empty);
                    boardState[x, y] = gameBoardState;
                }
                return boardState;
            });
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand(EXPECTED_COMMAND, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                if (response != null)
                {
                    var lines = response.Split(Environment.NewLine);
                    var boardSigns = lines.Skip(3).Take(TestProgram.BOARD_SIZE).SelectMany(l => l.Skip(3).Take(TestProgram.BOARD_SIZE));
                    boardSigns.Where(c => c == expectedMark).Should().HaveCount(10);
                }
            }
        }

        [Fact]
        internal void Handle_PrintsEmptyString_WhenResponseCommandIsNotRecognized()
        {
            _gameController.GetBoard().Returns(c =>
            {
                var boardState = new EGameBoardState[TestProgram.BOARD_SIZE, TestProgram.BOARD_SIZE];
                boardState[0, 0] = (EGameBoardState)100;
                return boardState;
            });
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand(EXPECTED_COMMAND, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                if (response != null)
                {
                    var lines = response.Split(Environment.NewLine);
                    var boardSigns = lines.Skip(3).Take(TestProgram.BOARD_SIZE).SelectMany(l => l.Skip(3).Take(TestProgram.BOARD_SIZE));
                    boardSigns.First().Should().Be(' ');
                }
            }
        }
    }
}
