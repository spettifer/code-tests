using FluentAssertions;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using Miqm.Battleships.Providers;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TestHelpCommandHandler
    {
        private const string CMD1_1 = "CMD1_1";
        private const string CMD1_2 = "CMD1_2";
        private const string CMD2_1 = "CMD2_1";
        private const string CMD2_2 = "CMD2_2";
        private const string CMD3 = "CMD3";
        private const string START_CMD = "START_CMD";

        private readonly HelpCommandHandler _subject;
        private readonly IProvideGameStartCommandText _gameStartCommandProvider;
        private readonly IProvideCommandHelpText[] _commandHelpTextProviders;

        public TestHelpCommandHandler()
        {
            _commandHelpTextProviders = new[] { Substitute.For<IProvideCommandHelpText>(), Substitute.For<IProvideCommandHelpText>(), Substitute.For<IProvideCommandHelpText>() };
            _commandHelpTextProviders[0].HandledCommandsList.Returns(new[] { CMD1_1, CMD1_2 });
            _commandHelpTextProviders[1].HandledCommandsList.Returns(new[] { CMD2_1, CMD2_2 });
            _commandHelpTextProviders[2].HandledCommandsList.Returns(new[] { CMD3 });
            _gameStartCommandProvider = Substitute.For<IProvideGameStartCommandText>();
            _gameStartCommandProvider.StartCommand.Returns(START_CMD);
            _subject = new HelpCommandHandler(_commandHelpTextProviders, _gameStartCommandProvider);
        }

        [Fact]
        internal void ShouldHandleHelpCommandByDisplayingAvailableCommands()
        {
            var handled = _subject.HandleCommand("help", out var result);
            handled.Should().BeTrue();
            result.Should().ContainAll(CMD1_1, CMD1_2, CMD2_1, CMD2_2, CMD3);
        }
        [Fact]
        internal void ShouldWelcomePlayerByDisplayingAvailableCommands()
        {
            _subject.WelcomeMessage.Should().ContainAll(CMD1_1, CMD1_2, CMD2_1, CMD2_2, CMD3);
        }
        [Fact]
        internal void ShouldWelcomePlayerByDisplayingCommandToStartGameWith()
        {
            _subject.WelcomeMessage.Should().Contain(START_CMD);
        }
        [Fact]
        internal void ShouldNotHandleWhenCommandIsNotHelp()
        {
            var handled = _subject.HandleCommand("?", out var result);
            handled.Should().BeFalse();
            result.Should().BeNull();
        }
        [Fact]
        internal void ShouldReturnHelpTextForUnknownCommandHanlder()
        {
            var response = _subject.HandleUnknownCommand("blabla");
            response.Should().NotBeNullOrWhiteSpace();
        }

    }
}
