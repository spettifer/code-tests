using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Execution;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Exceptions;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TestGameCoordinatesCommandHandler
    {
        private readonly IGameController _gameController;
        private readonly GameCoordinatesCommandHandler _subject;

        public TestGameCoordinatesCommandHandler()
        {
            _gameController = Substitute.For<IGameController>();
            _subject = new GameCoordinatesCommandHandler(_gameController);
        }

#nullable disable
        [Theory]
        [MemberData(nameof(ValidShots))]
        internal void Handle_ShouldReturnTrueAndPassCoordinatesToGameController_WhenCoordinateProvided(string command, Tuple<int, int> expectedCoordinates)
        {
            bool? result = null;
            string response = null;
            Action action = () => { result = _subject.HandleCommand(command, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                _gameController.Received(1).Shot(Arg.Is(expectedCoordinates));

            }
        }

        [Theory]
        [MemberData(nameof(InvalidShots))]
        internal void Handle_ShouldReturnFalseAndNotPassCoordinatesToGameController_WhenInvalidCoordinateProvided(string command)
        {
            bool? result = null;
            string response = null;
            Action action = () => { result = _subject.HandleCommand(command, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeFalse();
                _gameController.Received(0).Shot(Arg.Any<Tuple<int, int>>());

            }
        }

        public static IEnumerable<object[]> ValidShots
        {
            get
            {
                for (var x = 0; x < TestProgram.BOARD_SIZE; ++x)
                    for (var y = 0; y < TestProgram.BOARD_SIZE; ++y)
                    {
                        var shotCmd = "" + (char)('a' + x) + (y + 1);
                        yield return new object[] { shotCmd, Tuple.Create(x, y) };
                    }
            }
        }
        public static IEnumerable<object[]> InvalidShots
        {
            get
            {
                for (var z = 0; z < TestProgram.BOARD_SIZE; ++z)
                {
                    var r = new Random();
                    var x = r.Next('k', 'z' + 1);
                    var y = r.Next(TestProgram.BOARD_SIZE, 100);
                    var shotCmd = "" + (char)('a' + x) + (y + 1);
                    yield return new object[] { shotCmd };
                }
            }
        }


        [Theory]
        [MemberData(nameof(EGameResponseValues))]
        internal void Handle_ShouldReturnTextResponse_WhenGameControllerResponds(EGameResponse gameResponse)
        {
            string response = null;
            bool? result = null;
            _gameController.Shot(Arg.Any<Tuple<int, int>>()).Returns(gameResponse);
            Action action = () => { result = _subject.HandleCommand("c4", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrEmpty().And.NotBe(gameResponse.ToString());
            }
        }

        public static IEnumerable<object[]> EGameResponseValues => from v in Enum.GetValues(typeof(EGameResponse)).Cast<EGameResponse>()
                                                                   select new object[] { v };

#nullable enable

        [Fact]
        internal void Handle_ShouldReturnTextResponse_WhenGameControllerThrowsGameException()
        {
            string? response = null;
            bool? result = null;
            _gameController.ShotCount.Returns(50);
            _gameController.Shot(Arg.Any<Tuple<int, int>>()).Returns(EGameResponse.Over);
            Action action = () => { result = _subject.HandleCommand("c4", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().Contain("50");
            }
        }
        [Fact]
        internal void Handle_ShouldReturnTextResponseContainingAmountOfShotsPlaces_WhenGameControllerReportsGameOver()
        {
            string? response = null;
            bool? result = null;
            const string expectedExceptionText = "test Exception";
            _gameController.Shot(Arg.Any<Tuple<int, int>>()).Returns(c => { throw new GameException(expectedExceptionText); });
            Action action = () => { result = _subject.HandleCommand("c4", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().Contain(expectedExceptionText);
            }
        }
        [Fact]
        internal void Handle_PrintsWTF_WhenGameResponseIsNotRecognized()
        {
            const EGameResponse wtfState = (EGameResponse)100;
            _gameController.Shot(Arg.Any<Tuple<int, int>>()).Returns(wtfState);
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand("a1", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                if (response != null)
                {
                    response.Should().Contain("WTF").And.Contain(wtfState.ToString());
                }
            }
        }
    }
}
