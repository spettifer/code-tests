using System;
using FluentAssertions;
using FluentAssertions.Execution;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Exceptions;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TestGameStartCommandHandler
    {
        private readonly IGameController _gameController;
        private readonly GameStartCommandHandler _subject;

        public TestGameStartCommandHandler()
        {
            _gameController = Substitute.For<IGameController>();
            _subject = new GameStartCommandHandler(_gameController);
        }


        [Fact]
        internal void Handle_ShouldReturnFalseAndNoTextResponse_WhenCommandIsNotExpected()
        {
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand("begin", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                response.Should().BeNull();
                result.Should().NotBeNull().And.BeFalse();
            }
        }

        [Fact]
        internal void Handle_ShouldCallGameControllerNewGameAndReturnTrueAndTextResponse_WhenCommandIsExpected()
        {
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand("start", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                _gameController.Received(1).NewGame();
            }
        }

        [Fact]
        internal void Handle_ShouldReturnTrueAndTextResponse_WhenGameControllerNewGameThrowsGameException()
        {
            string? response = null;
            const string expectedExceptionText = "test Exception";
            bool? result = null;
            _gameController.When(s => s.NewGame()).Do(c => { throw new GameException(expectedExceptionText); });
            Action action = () => { result = _subject.HandleCommand("start", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().Contain(expectedExceptionText);
            }
        }
    }
}
