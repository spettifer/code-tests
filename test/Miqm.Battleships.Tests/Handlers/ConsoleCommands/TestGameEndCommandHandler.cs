using System;
using FluentAssertions;
using FluentAssertions.Execution;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Exceptions;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TestGameEndCommandHandler
    {
        private const string EXPECTED_COMMAND = "end";
        private readonly IGameController _gameController;
        private readonly GameEndCommandHandler _subject;

        public TestGameEndCommandHandler()
        {
            _gameController = Substitute.For<IGameController>();
            _subject = new GameEndCommandHandler(_gameController);
        }


        [Fact]
        internal void Handle_ShouldReturnFalseAndNoTextResponse_WhenCommandIsNotExpected()
        {
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand("finish", out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                response.Should().BeNull();
                result.Should().NotBeNull().And.BeFalse();
            }
        }

        [Fact]
        internal void Handle_ShouldCallGameControllerEndGameReturnTrueAndTextResponse_WhenCommandIsExpected()
        {
            string? response = null;
            bool? result = null;
            Action action = () => { result = _subject.HandleCommand(EXPECTED_COMMAND, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().NotBeNullOrWhiteSpace();
                _gameController.Received(1).EndGame();
            }
        }

        [Fact]
        internal void Handle_ShouldReturnTrueAndTextResponse_WhenGameControllerEndGameThrowsGameException()
        {
            string? response = null;
            const string expectedExceptionText = "test Exception";
            bool? result = null;
            _gameController.When(s => s.EndGame()).Do(c => { throw new GameException(expectedExceptionText); });
            Action action = () => { result = _subject.HandleCommand(EXPECTED_COMMAND, out response); };
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                result.Should().NotBeNull().And.BeTrue();
                response.Should().Contain(expectedExceptionText);
            }
        }
    }
}
