using System;
using System.Collections.Generic;
using System.Text;
using FluentAssertions;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Handlers.ConsoleCommands
{
    public class TextExitCommandHandler
    {
        private readonly IApplicationLifetime _appLifetime;
        private readonly ExitCommandHandler _subject;

        public TextExitCommandHandler()
        {
            _appLifetime = Substitute.For<IApplicationLifetime>();
            _subject = new ExitCommandHandler(_appLifetime);
        }

        [Fact]
        internal void Handle_ShouldNotCallStopAndReturnFalseAndTextResultNull_WhenCommandIsNotValid()
        {
            var handled = _subject.HandleCommand("quit", out var result);
            handled.Should().BeFalse();
            result.Should().BeNull();
        }

        [Fact]
        internal void Handle_ShouldCallStopAndReturnTrueAndTextResultSaySomething_WhenCommandIsExit()
        {
            var handled = _subject.HandleCommand("exit", out var result);
            handled.Should().BeTrue();
            result.Should().NotBeNullOrWhiteSpace();
            _appLifetime.Received(1).StopApplication();
        }
    }
}
