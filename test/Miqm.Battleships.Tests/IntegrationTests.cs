using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Controllers.Impl;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests
{
    public class IntegrationTests
    {
        [Fact]
#pragma warning disable IDE1006 // Naming Styles
        internal async Task Program_ShouldCompleteGame_ByWritingGameOverOnConsole_WhenUsingCommandInputsFromConsole()
#pragma warning restore IDE1006 // Naming Styles
        {
            var consoleIn = Substitute.For<TextReader>();
            var consoleOut = Substitute.For<TextWriter>();
            var consoleAccessProvider = Substitute.For<IProvideConsoleStreams>();
            consoleAccessProvider.ConsoleIn.Returns(consoleIn);
            consoleAccessProvider.ConsoleOut.Returns(consoleOut);
            consoleAccessProvider.ConsoleErr.Returns(Substitute.For<TextWriter>());
            using var tokenSource = new CancellationTokenSource();
            var gameHostBuilder = Program.ConfigureHost(new HostBuilder());
            gameHostBuilder.ConfigureContainer<ContainerBuilder>(builder =>
            {
                builder.Register(c => consoleAccessProvider).As<IProvideConsoleStreams>().InstancePerDependency();
            });
            var _gameHost = gameHostBuilder.Build();

            consoleOut.WriteLineAsync(Arg.Any<string>()).Returns(async c =>
             {
                 if (tokenSource.IsCancellationRequested)
                     return;
                 var line = c.ArgAt<string>(0);
                 if (line.Contains("Game Over", StringComparison.InvariantCultureIgnoreCase)
                 || line.StartsWith("!! Ups!", StringComparison.InvariantCultureIgnoreCase))
                     tokenSource.Cancel();
                 await Task.Delay(50);
             });
            var commands = new Queue<string>();
            commands.Enqueue("start");
            for (int i = 0; i < TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE; ++i)
            {
                commands.Enqueue($"{(char)('A' + (i % TestProgram.BOARD_SIZE))}{(i / TestProgram.BOARD_SIZE) + 1}");
            }
            consoleIn.ReadLineAsync().Returns(async c =>
            {
                await Task.Delay(50);
                if (tokenSource.Token.IsCancellationRequested)
                {
                    return null;
                }
                if (commands.TryDequeue(out var result))
                    return result;
                else
                {
                    tokenSource.Cancel();
                    return null;
                }
            });
            await _gameHost.StartAsync(tokenSource.Token);
            await _gameHost.WaitForShutdownAsync(tokenSource.Token);
            await _gameHost.StopAsync();
            consoleOut.Received(0).WriteLine(Arg.Is<string>(c => c.StartsWith("!! Ups!", StringComparison.InvariantCultureIgnoreCase)));
            consoleOut.Received(0).WriteLine(Arg.Is<string>(c => c.Contains("Unknown", StringComparison.InvariantCultureIgnoreCase)));
            consoleOut.Received(1).WriteLine(Arg.Is<string>(c => c.Contains("Game Over", StringComparison.InvariantCultureIgnoreCase)));
            consoleOut.Received(TestProgram.EXPECTED_SHIP_MARKS_COUNT - TestProgram.EXPECTED_SHIP_COUNT).WriteLine(Arg.Is<string>(c => c.Contains("hit", StringComparison.InvariantCultureIgnoreCase)));
            consoleOut.Received(TestProgram.EXPECTED_SHIP_COUNT - 1).WriteLine(Arg.Is<string>(c => c.Contains("sinked a ship", StringComparison.InvariantCultureIgnoreCase)));
        }

        [Fact]
        internal void GameController_ShouldCompleteGame()
        {
            var realGame = new GameController(new BoardController());
            realGame.NewGame();
            var responses = new List<EGameResponse>();
            void DoGame()
            {
                for (int x = 0; x < TestProgram.BOARD_SIZE; x++)
                {
                    for (int y = 0; y < TestProgram.BOARD_SIZE; y++)
                    {
                        var res = realGame.Shot(Tuple.Create(x, y));
                        responses.Add(res);
                        if (res == EGameResponse.Over)
                            return;
                    }
                }
            }
            DoGame();
            using (new AssertionScope())
            {
                responses.Where(r => r == EGameResponse.Over).Should().HaveCount(1, because: "Game should be reported as over only once (when it's over)");
                responses.Where(r => r == EGameResponse.Hit).Should().HaveCount(TestProgram.EXPECTED_SHIP_MARKS_COUNT - TestProgram.EXPECTED_SHIP_COUNT, "Hit is reported when part of ship is hit.");
                responses.Where(r => r == EGameResponse.Sink).Should().HaveCount(2, because: "Sink is reported when last piece of ship is hit, except when it's the last ship, then it's game over");
                responses.Where(r => r == EGameResponse.Miss).Should().HaveCount(responses.Count - TestProgram.EXPECTED_SHIP_MARKS_COUNT, because: "all shots besides where ships are should be reported as miss");
            }
        }
    }
}
