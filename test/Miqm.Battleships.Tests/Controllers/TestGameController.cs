using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Controllers.Impl;
using Miqm.Battleships.Exceptions;
using NSubstitute;
using Xunit;

namespace Miqm.Battleships.Tests.Controllers
{
    public class TestGameController
    {
        private readonly IBoardController _boardController;
        private readonly GameController _subject;

        public TestGameController()
        {
            _boardController = Substitute.For<IBoardController>();
            _boardController.BoardSize.Returns(TestProgram.BOARD_SIZE);
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject = new GameController(_boardController);
        }

        [Fact]
        internal void Shot_ShouldThrowGameException_WhenGameWasNotStarted()
        {
            Action action = () => { _subject.Shot(Tuple.Create(0, 0)); };
            action.Should().Throw<GameException>();
        }
        [Fact]
        internal void Shot_ShouldReportSinkAndGameOver_WhenShipsAreSunk()
        {
            var marks = new Queue<int?>();
            marks.Enqueue(null);
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
                if (c.ArgAt<int>(0) > 0)
                    marks.Enqueue(c.ArgAt<int>(0));
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            var expectedMarks = marks.ToArray();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return marks.Dequeue();
            });
            var y = 0;
            var x = 0;
            var firstShot = _subject.Shot(Tuple.Create(x, y));
            firstShot.Should().Be(EGameResponse.Miss);

            for (int i = 1; i < expectedMarks.Length - 1; i++)
            {
                x++; if (x >= TestProgram.BOARD_SIZE) { x = 0; y++; }

                var shot = _subject.Shot(Tuple.Create(x, y));
                if (expectedMarks[i + 1] != expectedMarks[i])
                {
                    shot.Should().Be(EGameResponse.Sink);
                }
                else
                {
                    shot.Should().Be(EGameResponse.Hit);
                }
            }
            x++; if (x >= TestProgram.BOARD_SIZE) { x = 0; y++; }
            var lastShot = _subject.Shot(Tuple.Create(x, y));
            lastShot.Should().Be(EGameResponse.Over);
        }
        [Theory]
        [InlineData(0, EGameResponse.Duplicate)]
        [InlineData(1, EGameResponse.Hit)]
        [InlineData(-1, EGameResponse.Duplicate)]
        [InlineData(null, EGameResponse.Miss)]
        internal void Shot_ShouldReturnHitMissOrDuplicate_AccordingToBoardState(int? boardMark, EGameResponse expectedResponse)
        {
            _subject.NewGame();
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return boardMark;
            });
            var y = 0;
            var x = 0;
            var shot = _subject.Shot(Tuple.Create(x, y));
            shot.Should().Be(expectedResponse);
        }
        [Fact]
        internal void Shot_ShouldIncreaseShotCount()
        {
            _subject.NewGame();
            var countBefore = _subject.ShotCount;
            Action action = () => { _subject.Shot(Tuple.Create(0, 0)); };
            action.Should().NotThrow();
            _subject.ShotCount.Should().Be(countBefore + 1);
        }
        [Fact]
        internal void Shot_ShouldNotIncreaseShotCount_WhenShotIsDuplicate()
        {
            var countBefore = _subject.ShotCount;
            _subject.NewGame();
            var marks = new Queue<int?>();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
                marks.Enqueue(c.ArgAt<int>(0));
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return marks.TryDequeue(out var r) ? r : null;
            });
            _subject.Shot(Tuple.Create(0, 0));
            _subject.Shot(Tuple.Create(0, 0));
            _subject.ShotCount.Should().Be(countBefore + 1);
        }
        [Fact]
        internal void NewGame_ShouldGenerateNewMapWith3Ships_WhenGameWasNotStarted()
        {
            _subject.NewGame();
            _boardController.Received(1).ClearBoard();
            _boardController.Received(TestProgram.EXPECTED_SHIP_MARKS_COUNT).MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
        }
        [Fact]
        internal void NewGame_OnPlacingShips_ShouldRepeatShipPlacement_WhenRandomizedPlaceIsOccupied()
        {
            var fields = new Queue<int?>();
            fields.Enqueue(1);
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return fields.TryDequeue(out var r) ? r : null;
            });
            _subject.NewGame();
            _boardController.Received(1).ClearBoard();
            _boardController.Received(TestProgram.EXPECTED_SHIP_MARKS_COUNT + 1).CheckField(Arg.Any<int>(), Arg.Any<int>());
            _boardController.Received(TestProgram.EXPECTED_SHIP_MARKS_COUNT).MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>());
        }
        [Fact]
        internal void NewGame_ShouldThrowGameException_WhenGameWasStarted()
        {
            Action action = () => { _subject.NewGame(); };
            action.Should().NotThrow();
            action.Should().Throw<GameException>();
        }
        [Fact]
        internal void NewGame_ShouldClearShotCount_WhenGameWasStarted()
        {
            _subject.NewGame();
            _subject.Shot(Tuple.Create(0, 0));
            _subject.Shot(Tuple.Create(0, 1));
            _subject.Shot(Tuple.Create(0, 2));
            _subject.EndGame();
            _subject.NewGame();
            _subject.ShotCount.Should().Be(0);
        }

        private static IEnumerable<EGameBoardState> BoardElements(EGameBoardState[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    yield return board[i, j];
                }
            }
        }
        [Fact]
        internal void GetBoard_ShouldNotDiscloseShipsLocationsMarkedWithPositiveValues_WhenGameIsOngoing()
        {
            var marks = new Queue<int?>();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
                if (c.ArgAt<int>(0) > 0)
                    marks.Enqueue(c.ArgAt<int>(0));
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return marks.TryDequeue(out var r) ? r : null;
            });
            var elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.Where(s => s == EGameBoardState.ShipRemaining).Should().BeEmpty();
        }

        [Fact]
        internal void GetBoard_ShouldDiscloseRemainingShipsLocationsMarkedWithPositiveValues_IfGameWasEnded()
        {
            var marks = new Queue<int?>();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
                if (c.ArgAt<int>(0) > 0)
                    marks.Enqueue(c.ArgAt<int>(0));
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            var expetedMarksCount = marks.Count;
            _subject.EndGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return marks.TryDequeue(out var r) ? r : null;
            });
            var elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.Where(s => s == EGameBoardState.ShipRemaining).Should().HaveCount(expetedMarksCount);
        }
        [Fact]
        internal void GetBoard_ShouldReturnNegativeBoardMarksAsShipHits_RegardlessGameState()
        {
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return -1;
            });
            var elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Hit).Should().BeTrue();
            _subject.EndGame();
            elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Hit).Should().BeTrue();
        }
        [Fact]
        internal void GetBoard_ShouldReturnZeroBoardMarksAsMisses_RegardlessGameState()
        {
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return 0;
            });
            var elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Miss).Should().BeTrue();
            _subject.EndGame();
            elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Miss).Should().BeTrue();
        }
        [Fact]
        internal void GetBoard_ShouldReturnNullBoardMarksAsEmpty_RegardlessGameState()
        {
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            var elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Empty).Should().BeTrue();
            _subject.EndGame();
            elements = BoardElements(_subject.GetBoard());
            elements.Should().HaveCount(TestProgram.BOARD_SIZE * TestProgram.BOARD_SIZE).And.Subject.All(s => s == EGameBoardState.Empty).Should().BeTrue();
        }

        [Fact]
        internal void EndGame_ShouldCompleteGame_WhenItWasNotOver()
        {
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            Action endGameAction = () => { _subject.EndGame(); };
            Action shootAction = () => { _subject.Shot(Tuple.Create(0, 0)); };
            Action newGameAction = () => { _subject.NewGame(); };
            endGameAction.Should().NotThrow<GameException>();
            shootAction.Should().Throw<GameException>();
            newGameAction.Should().NotThrow<GameException>();
        }

        [Fact]
        internal void EndGame_ShouldThrowException_WhenGameWasNotStarted()
        {
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            Action endGameAction = () => { _subject.EndGame(); };
            endGameAction.Should().Throw<GameException>();
        }
        [Fact]
        internal void EndGame_ShouldThrowException_WhenGameWasCompleted()
        {
            var marks = new Queue<int?>();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
                if (c.ArgAt<int>(0) > 0)
                    marks.Enqueue(c.ArgAt<int>(0));
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return null;
            });
            _subject.NewGame();
            _boardController.When(c => c.MarkField(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<int>())).Do(c =>
            {
            });
            _boardController.CheckField(Arg.Any<int>(), Arg.Any<int>()).Returns(c =>
            {
                return marks.Dequeue();
            });
            while (_subject.Shot(Tuple.Create(0, 0)) != EGameResponse.Over) ;
            Action endGameAction = () => { _subject.EndGame(); };
            endGameAction.Should().Throw<GameException>();
        }
    }
}
