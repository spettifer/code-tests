using System;
using FluentAssertions;
using Miqm.Battleships.Controllers.Impl;
using Miqm.Battleships.Exceptions;
using Xunit;

namespace Miqm.Battleships.Tests.Controllers
{

    public class TestBoardController
    {
        private readonly BoardController _subject;

        public TestBoardController()
        {
            _subject = new BoardController();
        }

        [Fact]
        internal void MarkField_ShouldOverrideMarkPlacedBefore()
        {
            _subject.MarkField(1, 0, 0);
            _subject.MarkField(2, 0, 0);
            var result = _subject.CheckField(0, 0);
            result.Should().Be(2);
        }

        [Fact]
        internal void CheckField_ShouldReturnTheMarkWhichWasSet()
        {
            _subject.MarkField(1, 0, 0);
            var result = _subject.CheckField(0, 0);
            result.Should().Be(1);
        }
        [Fact]
        internal void ClearBoard_ShouldClearBoard()
        {
            _subject.MarkField(1, 0, 0);
            var result = _subject.CheckField(0, 0);
            result.Should().Be(1);
            _subject.ClearBoard();
            result = _subject.CheckField(0, 0);
            result.Should().BeNull();
        }
        [Fact]
        internal void CheckField_ShouldReturnNull_WhenMarkWasNotSet()
        {
            _subject.MarkField(1, 0, 0);
            var result = _subject.CheckField(1, 1);
            result.Should().BeNull();
        }

        [Fact]
        internal void CheckField_ThrowsException_WhenCoordinatesOutsideBoard()
        {
            Action check = () => { _subject.CheckField(TestProgram.BOARD_SIZE, TestProgram.BOARD_SIZE); };
            check.Should().Throw<GameException>();
        }
        [Fact]
        internal void MarkField_ThrowsException_WhenCoordinatesOutsideBoard()
        {
            Action mark = () => { _subject.MarkField(1, TestProgram.BOARD_SIZE, TestProgram.BOARD_SIZE); };
            mark.Should().Throw<GameException>();
        }
        [Fact]
        internal void BoardSize_ShouldBe10()
        {
            _subject.BoardSize.Should().Be(TestProgram.BOARD_SIZE);
        }
    }
}
