using System;
using System.Linq;
using FluentAssertions;
using Miqm.Battleships.Handlers.ConsoleCommands.Impl;
using Miqm.Battleships.Providers;
using Xunit;

namespace Miqm.Battleships.Tests.Providers
{
    public class TestCommandHelpTextProviders
    {

        void Assert(IProvideCommandHelpText _subject)
        {
            _subject.HandledCommandsList.Should().NotBeEmpty().And.Subject.All(s => !string.IsNullOrEmpty(s)).Should().BeTrue();
        }

        [Theory]
        [InlineData(typeof(HelpCommandHandler.HelpTextProvider))]
        [InlineData(typeof(GameCoordinatesCommandHandler.HelpTextProvider))]
        internal void HandledCommandsList_ShouldReturnListOfNonEmptyHelpTexts(Type helpTextProviderType)
        {
            var obj = Activator.CreateInstance(helpTextProviderType) as IProvideCommandHelpText;
            obj.Should().NotBeNull();
            if (obj != null) Assert(obj);
        }
    }
}
