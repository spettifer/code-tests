using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Handlers;
using Miqm.Battleships.Handlers.ConsoleCommands;
using Miqm.Battleships.Providers;
using Xunit;

namespace Miqm.Battleships.Tests
{
    public class TestProgram
    {
        internal const int BOARD_SIZE = 10;
        internal const int EXPECTED_SHIP_MARKS_COUNT = 2 * 4 + 1 * 5;
        internal const int EXPECTED_SHIP_COUNT = 3;

        private readonly IHost _subject;

        public TestProgram()
        {
            _subject = Program.ConfigureHost(new HostBuilder()).Build();
        }
        [Theory]
        [InlineData(typeof(IGameController))]
        internal void ShouldHaveOnlyOneServiceOfType_ForApplicationLifetime(Type serviceType)
        {
            using var scope1 = _subject.Services.GetService<ILifetimeScope>();
            scope1.IsRegistered(serviceType).Should().BeTrue();
            var s1 = scope1.ResolveOptional(serviceType);
            using var scope2 = scope1.BeginLifetimeScope();
            var s2 = scope2.ResolveOptional(serviceType);
            using (new AssertionScope())
            {
                s2.Should().NotBeNull();
                s1.Should().NotBeNull().And.Subject.GetHashCode().Should().Be(s2.GetHashCode());
            }
        }

        [Theory]
        [InlineData(typeof(IHandleConsoleInput))]
        [InlineData(typeof(IHandleUnknownCommand))]
        [InlineData(typeof(IProvideWelcomeMessage))]
        internal void ShouldHaveServiceOfType_AndItCanBeResolved(Type serviceType)
        {
            using var scope1 = _subject.Services.GetService<ILifetimeScope>();
            scope1.IsRegistered(serviceType).Should().BeTrue();
            object? service = null;
            Action action = () => service = scope1.ResolveOptional(serviceType);
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                service.Should().NotBeNull();
            }
        }
        [Theory]
        [InlineData(typeof(IEnumerable<IProvideCommandHelpText>))]
        [InlineData(typeof(IEnumerable<IHandleCommand>))]
        internal void ShouldHaveAtLeastOneServiceOfType_ThatCanBeResolved(Type serviceType)
        {
            using var scope1 = _subject.Services.GetService<ILifetimeScope>();
            scope1.IsRegistered(serviceType).Should().BeTrue();
            object? service = null;
            Action action = () => service = scope1.ResolveOptional(serviceType);
            using (new AssertionScope())
            {
                action.Should().NotThrow();
                service.Should().NotBeNull().And.Subject.As<IEnumerable>().Should().NotBeEmpty();
            }
        }

        [Fact]
        internal void ShouldHaveCommandsWithUniqueOrderValue()
        {
            using var scope = _subject.Services.GetService<ILifetimeScope>();
            var commands = scope.Resolve<IEnumerable<IHandleCommand>>().ToList();
            commands.Select(c => c.Order).Distinct().Should().HaveCount(commands.Count);
        }
    }
}
