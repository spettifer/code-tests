using System;
using System.Collections.Generic;
using System.Linq;
using Miqm.Battleships.Exceptions;

namespace Miqm.Battleships.Controllers.Impl
{
    class GameController : IGameController
    {
        private int BoardSize => _boardController.BoardSize;

        public int ShotCount { get; private set; }

        private readonly Random _random = new Random();

        private static readonly Ship[] GAME_SHIPS = new[] {
            new Ship() { id = 1, elementsCount = 5 },
            new Ship() { id = 2, elementsCount = 4 },
            new Ship() { id = 3, elementsCount = 4 }
        };

        private class Ship
        {
            internal int id;
            internal int elementsCount;
            internal Ship Clone()
            {
                return new Ship() { id = id, elementsCount = elementsCount };
            }
        }

        private readonly List<Ship> _ships = new List<Ship>();
        private readonly IBoardController _boardController;

        private bool _gameEnded = true;

        private bool IsGameOver()
        {
            return _ships.All(IsShipSunk);
        }
        private static bool IsShipSunk(Ship ship)
        {
            return ship.elementsCount == 0;
        }

        public GameController(IBoardController boardController)
        {
            _boardController = boardController;
        }

        public void NewGame()
        {
            if (!_gameEnded)
                throw new GameException("Cannot start a new game while current is still ongoing.");
            _boardController.ClearBoard();
            _ships.Clear();
            ShotCount = 0;
            foreach (var ship in GAME_SHIPS)
            {
                var newShip = ship.Clone();
                _ships.Add(newShip);
                while (!TrySetupShip(newShip)) ; //empty, all logic in while clause, should exit after first successfull setup;
            }
            _gameEnded = false;
        }

        private bool TrySetupShip(Ship ship)
        {
            var shipCoords = GetRandomShipCoordinates(ship);
            foreach (var c in shipCoords)
            {
                if (_boardController.CheckField(c.Item1, c.Item2) != null)
                    return false;
            }
            foreach (var c in shipCoords)
            {
                _boardController.MarkField(ship.id, c.Item1, c.Item2);
            }
            return true;
        }

        private Tuple<int, int>[] GetRandomShipCoordinates(Ship ship)
        {
            var isHorizontal = _random.Next(0, 2) == 0;
            var startX = _random.Next(0, isHorizontal ? BoardSize - ship.elementsCount : BoardSize);
            var startY = _random.Next(0, isHorizontal ? BoardSize : BoardSize - ship.elementsCount);
            var shipCoords = new Tuple<int, int>[ship.elementsCount];
            for (int i = 0; i < ship.elementsCount; i++)
            {
                shipCoords[i] = Tuple.Create(startX + (isHorizontal ? i : 0), startY + (isHorizontal ? 0 : i));
            }

            return shipCoords;
        }

        public EGameResponse Shot(Tuple<int, int> shootCoordinates)
        {
            if (_gameEnded)
                throw new GameException("Game is not started.");
            var mark = _boardController.CheckField(shootCoordinates.Item1, shootCoordinates.Item2);
            if (mark == null)
            {
                _boardController.MarkField(0, shootCoordinates.Item1, shootCoordinates.Item2);
                ShotCount++;
                return EGameResponse.Miss;
            }
            var shipId = mark.Value;
            if (shipId <= 0)
                return EGameResponse.Duplicate;
            ShotCount++;
            var ship = _ships.Single(s => s.id == shipId);
            ship.elementsCount--;
            _boardController.MarkField(0 - shipId, shootCoordinates.Item1, shootCoordinates.Item2);
            if (IsGameOver())
            {
                _gameEnded = true;
                return EGameResponse.Over;
            }
            if (IsShipSunk(ship))
                return EGameResponse.Sink;
            return EGameResponse.Hit;
        }

        public EGameBoardState[,] GetBoard()
        {
            var boardState = new EGameBoardState[_boardController.BoardSize, _boardController.BoardSize];
            for (int x = 0; x < _boardController.BoardSize; x++)
            {
                for (int y = 0; y < _boardController.BoardSize; y++)
                {
                    boardState[x, y] = TranslateToBoardState(_boardController.CheckField(x, y), userBoard: !_gameEnded);
                }
            }
            return boardState;
        }

        public void EndGame()
        {
            if (_gameEnded)
                throw new GameException("Game is not started.");
            _gameEnded = true;
        }

        private EGameBoardState TranslateToBoardState(int? mark, bool userBoard)
        {
            if (mark == null)
                return EGameBoardState.Empty;
            if (mark == 0)
                return EGameBoardState.Miss;
            if (mark < 0)
                return EGameBoardState.Hit;
            return userBoard ? EGameBoardState.Empty : EGameBoardState.ShipRemaining;
        }

    }
}
