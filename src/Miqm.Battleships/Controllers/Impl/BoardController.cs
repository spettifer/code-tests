using Miqm.Battleships.Exceptions;

namespace Miqm.Battleships.Controllers.Impl
{
    class BoardController : IBoardController
    {
        private const int BOARD_SIZE = 10;
        private readonly int?[,] _board = new int?[BOARD_SIZE, BOARD_SIZE];

        public int BoardSize => BOARD_SIZE;

        public int? CheckField(int x, int y)
        {
            if (x < 0 || x >= BOARD_SIZE || y < 0 || y >= BOARD_SIZE)
                throw new GameException($"Coordinates canot be outside {BOARD_SIZE}x{BOARD_SIZE} board.");
            return _board[x, y];
        }

        public void ClearBoard()
        {
            for (int i = 0; i < BOARD_SIZE; i++)
            {
                for (int j = 0; j < BOARD_SIZE; j++)
                {
                    _board[i, j] = null;
                }
            }
        }

        public void MarkField(int mark, int x, int y)
        {
            if (x < 0 || x >= BOARD_SIZE || y < 0 || y >= BOARD_SIZE)
                throw new GameException($"Coordinates canot be outside {BOARD_SIZE}x{BOARD_SIZE} board.");
            _board[x, y] = mark;
        }
    }
}
