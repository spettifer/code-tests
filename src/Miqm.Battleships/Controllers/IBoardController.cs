namespace Miqm.Battleships.Controllers
{
    interface IBoardController
    {
        int BoardSize { get; }
        void ClearBoard();
        void MarkField(int mark, int x, int y);
        int? CheckField(int x, int y);
    }
}
