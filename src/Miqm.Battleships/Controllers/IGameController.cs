using System;

namespace Miqm.Battleships.Controllers
{
    interface IGameController
    {
        int ShotCount { get; }

        void NewGame();

        EGameResponse Shot(Tuple<int, int> shootCoordinates);

        EGameBoardState[,] GetBoard();

        void EndGame();
    }

    enum EGameResponse
    {
        Hit,
        Miss,
        Sink,
        Over,
        Duplicate
    }
    enum EGameBoardState
    {
        Empty,
        Miss,
        Hit,
        ShipRemaining
    }

}
