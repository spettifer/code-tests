using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Handlers;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Services
{
    internal class BattleshipsConsoleGameService : BackgroundService
    {
        private const string RESPONSE_PROMPT = "# ";
        private const string INPUT_PROMPT = "$> ";
        private readonly IProvideConsoleStreams _consoleAccessProvider;
        private readonly IHandleConsoleInput _consoleInputHandler;
        private readonly IProvideWelcomeMessage _welcomeMessageProvider;

        public BattleshipsConsoleGameService(IProvideConsoleStreams consoleAccessProvider, IHandleConsoleInput consoleInputHandler, IProvideWelcomeMessage welcomeMessageProvider)
        {
            _consoleAccessProvider = consoleAccessProvider;
            _consoleInputHandler = consoleInputHandler;
            _welcomeMessageProvider = welcomeMessageProvider;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(async () =>
            {
                //await Task.Delay(100);
                _consoleAccessProvider.Clear();
                _consoleAccessProvider.ConsoleOut.WriteLine(_welcomeMessageProvider.WelcomeMessage);
                while (!stoppingToken.IsCancellationRequested)
                {
                    try
                    {
                        _consoleAccessProvider.ConsoleOut.Write(INPUT_PROMPT);
                        var c = await _consoleAccessProvider.ConsoleIn.ReadLineAsync();
                        //await Task.Delay(50);
                        if (!string.IsNullOrWhiteSpace(c))
                        {
                            var response = _consoleInputHandler.HandleConsoleInput(c);
                            if (!string.IsNullOrEmpty(response))
                                _consoleAccessProvider.ConsoleOut.WriteLine(RESPONSE_PROMPT + response);
                        }
                        else
                        {
                            _consoleAccessProvider.ConsoleOut.WriteLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        _consoleAccessProvider.ConsoleErr.WriteLine($"!! Ups! Something went wrong: {ex.Message}. Please try to turn it off and on again.");
                    }
                }
            });
        }
    }
}
