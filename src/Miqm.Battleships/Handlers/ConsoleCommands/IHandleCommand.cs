namespace Miqm.Battleships.Handlers.ConsoleCommands
{
    interface IHandleCommand
    {
        int Order { get; }
        bool HandleCommand(string consoleInput, out string? result);
    }
}
