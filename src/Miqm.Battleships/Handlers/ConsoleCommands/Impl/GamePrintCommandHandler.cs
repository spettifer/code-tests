using System.Collections.Generic;
using System.Text;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class GamePrintCommandHandler : GameCommandHandlerBase
    {
        private const string COMMAND = "print";
        private const char HIT_MARK = '#';
        private const char MISS_MARK = '.';
        private const char SHIP_MARK = '%';
        private const char EMPTY_MARK = ' ';

        internal class HelpTextProvider : IProvideCommandHelpText
        {

            public IEnumerable<string> HandledCommandsList { get; } = new[] {
                COMMAND
            };
        }

        public GamePrintCommandHandler(IGameController gameController) : base(gameController)
        {
        }

        public override int Order => 4;

        protected internal override bool HandleCommandInternal(string consoleInput, out string? result)
        {
            result = null;
            if (string.Equals(consoleInput, COMMAND))
            {
                result = DrawBoard(_gameController.GetBoard());
                return true;
            }
            return false;
        }

        private string DrawBoard(EGameBoardState[,] gameBoard)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{MISS_MARK} - missed shot, {HIT_MARK} - ship hit, {SHIP_MARK} - ship not hit");
            sb.Append("  |"); //place for row header
            for (int x = 0; x < gameBoard.GetLength(0); x++)
            {
                sb.Append((char)('A' + x));
            }
            sb.AppendLine("|");
            sb.Append("--+"); //place for row header
            sb.Append(new string('-', gameBoard.GetLength(0)));
            sb.AppendLine("|");
            for (int y = 0; y < gameBoard.GetLength(1); y++)
            {
                var rowNo = y + 1;
                if (rowNo < 10)
                    sb.Append(' ');
                sb.Append(rowNo);
                sb.Append("|"); //place for row header
                for (int x = 0; x < gameBoard.GetLength(0); x++)
                {
                    sb.Append(GetBoardMark(gameBoard[x, y]));
                }
                sb.AppendLine("|");
            }
            sb.Append("---"); //place for row header
            sb.Append(new string('-', gameBoard.GetLength(0)));
            sb.AppendLine("-");
            return sb.ToString();
        }

        private static char GetBoardMark(EGameBoardState gameBoardState) => gameBoardState switch
        {
            EGameBoardState.Empty => EMPTY_MARK,
            EGameBoardState.Hit => HIT_MARK,
            EGameBoardState.Miss => MISS_MARK,
            EGameBoardState.ShipRemaining => SHIP_MARK,
            _ => EMPTY_MARK
        };
    }
}
