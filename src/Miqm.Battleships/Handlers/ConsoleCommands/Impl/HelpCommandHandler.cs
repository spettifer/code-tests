using System;
using System.Collections.Generic;
using System.Linq;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class HelpCommandHandler : IHandleCommand, IHandleUnknownCommand, IProvideWelcomeMessage
    {
        private const string HELP_COMMAND = "help";

        internal class HelpTextProvider : IProvideCommandHelpText
        {
            public IEnumerable<string> HandledCommandsList { get; } = new[] { HELP_COMMAND };
        }

        public string WelcomeMessage => $"Welcome to Miq's Battleships!{Environment.NewLine}Available commands are: {GetHelpCommands()}.{Environment.NewLine}Type '{_gameStartCommandProvider.StartCommand}' to start a new game.";

        private string GetHelpCommands()
        {
            return string.Join(", ", _commandHelpTexts.Select(c => string.Join(" or ", c.HandledCommandsList)));
        }

        private readonly IEnumerable<IProvideCommandHelpText> _commandHelpTexts;
        private readonly IProvideGameStartCommandText _gameStartCommandProvider;

        public HelpCommandHandler(IEnumerable<IProvideCommandHelpText> commandHelpTexts, IProvideGameStartCommandText gameStartCommandProvider)
        {
            _commandHelpTexts = commandHelpTexts;
            _gameStartCommandProvider = gameStartCommandProvider;
        }
        public int Order => 100;

        public bool HandleCommand(string consoleInput, out string? result)
        {
            if (!string.Equals(HELP_COMMAND, consoleInput))
            {
                result = null;
                return false;
            };
            result = $"Available commands are: {GetHelpCommands()}.";
            return true;
        }

        public string HandleUnknownCommand(string consoleInput)
        {
            return $"Unknown command '{consoleInput}'. Type 'help' to get list of available commands.";
        }


    }
}
