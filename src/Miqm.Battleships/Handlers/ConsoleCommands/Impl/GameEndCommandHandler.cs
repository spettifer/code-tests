using System.Collections.Generic;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class GameEndCommandHandler : GameCommandHandlerBase
    {
        private const string COMMAND = "end";
        internal class HelpTextProvider : IProvideCommandHelpText
        {

            public IEnumerable<string> HandledCommandsList { get; } = new[] {
                COMMAND
            };
        }

        public GameEndCommandHandler(IGameController gameController) : base(gameController)
        {
        }

        public override int Order => 3;

        protected internal override bool HandleCommandInternal(string consoleInput, out string? result)
        {
            result = null;
            if (string.Equals(consoleInput, COMMAND))
            {
                _gameController.EndGame();
                result = "Game finished.";
                return true;
            }
            return false;
        }
    }
}
