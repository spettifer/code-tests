using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class GameCoordinatesCommandHandler : GameCommandHandlerBase
    {
        internal class HelpTextProvider : IProvideCommandHelpText
        {
            public IEnumerable<string> HandledCommandsList { get; } = new[] {
                $"Coordinates on grid in form of [A-J][1-10] (eg. 'I10' or 'A4')"
            };
        }

        public GameCoordinatesCommandHandler(IGameController gameController) : base(gameController)
        {
        }

        public override int Order => 2;

        const string SHOOT_REGEX = "^[a-z]([2-9]|10?)$";
        protected internal override bool HandleCommandInternal(string consoleInput, out string? result)
        {
            result = null;

            var shootRegex = new Regex(SHOOT_REGEX);
            if (!shootRegex.IsMatch(consoleInput))
                return false;

            var coordinates = GetCoordinates(consoleInput);
            var response = _gameController.Shot(coordinates);
            result = TranslateResponseToTextResult(response);

            return true;
        }

        private Tuple<int, int> GetCoordinates(string command)
        {
            return Tuple.Create(command[0] - 'a', int.Parse(command.Substring(1)) - 1);
        }

        private string TranslateResponseToTextResult(EGameResponse response)
        {
            return response switch
            {
                EGameResponse.Hit => "Bam! It's a hit!",
                EGameResponse.Miss => "Opps! Missed...",
                EGameResponse.Sink => "Yeah! You sinked a ship! Keep shooting!",
                EGameResponse.Over => $"Game Over! You shot {_gameController.ShotCount} times to sunk all ships. Congratulations!",
                EGameResponse.Duplicate => "Really? You shoot here before...",
                _ => $"WTF? ({response.ToString()})",
            };
        }
    }
}
