using System.Collections.Generic;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class GameStartCommandHandler : GameCommandHandlerBase
    {
        private const string START_COMMAND = "start";
        internal class HelpTextProvider : IProvideCommandHelpText, IProvideGameStartCommandText
        {

            public IEnumerable<string> HandledCommandsList { get; } = new[] {
                START_COMMAND
            };

            public string StartCommand => START_COMMAND;
        }

        public GameStartCommandHandler(IGameController gameController) : base(gameController)
        {
        }

        public override int Order => 1;

        protected internal override bool HandleCommandInternal(string consoleInput, out string? result)
        {
            result = null;
            if (string.Equals(consoleInput, START_COMMAND))
            {
                _gameController.NewGame();
                result = "Starting a new game. 3 Ships are placed on board: 1x Battleship (5 squares) and 2x Destroyers (4 squares). Begin entering coordinates to shot. Type 'print' to draw current board state.";
                return true;
            }
            return false;
        }
    }
}
