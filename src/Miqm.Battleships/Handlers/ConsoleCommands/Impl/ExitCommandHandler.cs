using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Providers;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    class ExitCommandHandler : IHandleCommand
    {
        private const string COMMAND = "exit";

        internal class HelpTextProvider : IProvideCommandHelpText
        {
            public IEnumerable<string> HandledCommandsList { get; } = new[] { COMMAND };
        }

        private readonly IApplicationLifetime _applicationLifetime;

        public ExitCommandHandler(IApplicationLifetime applicationLifetime)
        {
            _applicationLifetime = applicationLifetime;
        }
        public int Order => 200;

        public bool HandleCommand(string consoleInput, out string? result)
        {
            if (!string.Equals(COMMAND, consoleInput))
            {
                result = null;
                return false;
            };
            result = $"Bye!";
            _applicationLifetime.StopApplication();
            Task.Delay(200).GetAwaiter().GetResult();
            return true;
        }
    }
}
