using Miqm.Battleships.Controllers;
using Miqm.Battleships.Exceptions;

namespace Miqm.Battleships.Handlers.ConsoleCommands.Impl
{
    abstract class GameCommandHandlerBase : IHandleCommand
    {
        protected readonly IGameController _gameController;

        protected internal GameCommandHandlerBase(IGameController gameController)
        {
            _gameController = gameController;
        }
        public abstract int Order { get; }

        public bool HandleCommand(string consoleInput, out string? result)
        {
            try
            {
                return HandleCommandInternal(consoleInput, out result);
            }
            catch (GameException ge)
            {
                result = $"I'm sorry, but {ge.Message} Please try again.";
                return true;
            }
        }

        protected internal abstract bool HandleCommandInternal(string consoleInput, out string? result);
    }
}
