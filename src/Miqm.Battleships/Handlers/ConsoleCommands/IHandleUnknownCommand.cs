namespace Miqm.Battleships.Handlers.ConsoleCommands
{
    interface IHandleUnknownCommand
    {
        string HandleUnknownCommand(string consoleInput);
    }
}
