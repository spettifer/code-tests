using System.Collections.Generic;
using System.Linq;
using Miqm.Battleships.Handlers.ConsoleCommands;

namespace Miqm.Battleships.Handlers.Impl
{
    class ConsoleInputHandler : IHandleConsoleInput
    {
        private readonly IEnumerable<IHandleCommand> _commandHandlers;
        private readonly IHandleUnknownCommand _unknownCommandHandler;

        public ConsoleInputHandler(IEnumerable<IHandleCommand> commandHandlers, IHandleUnknownCommand unknownCommandHandler)
        {
            _commandHandlers = commandHandlers;
            _unknownCommandHandler = unknownCommandHandler;
        }
        public string HandleConsoleInput(string input)
        {
            var command = input.Trim().ToLowerInvariant();
            foreach (var handler in _commandHandlers.OrderBy(h => h.Order).ToList())
            {
                if (handler.HandleCommand(command, out var result))
                {
                    return result ?? string.Empty;
                }
            }
            return _unknownCommandHandler.HandleUnknownCommand(command);
        }
    }
}
