namespace Miqm.Battleships.Handlers
{
    interface IHandleConsoleInput
    {
        string HandleConsoleInput(string input);
    }
}
