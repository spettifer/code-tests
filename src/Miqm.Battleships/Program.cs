using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Miqm.Battleships.Controllers;
using Miqm.Battleships.Controllers.Impl;
using Miqm.Battleships.Providers.Impl;
using Miqm.Battleships.Services;

namespace Miqm.Battleships
{
    [ExcludeFromCodeCoverage]
    class Program
    {
#pragma warning disable IDE1006 // Naming Styles
        static async Task Main()
#pragma warning restore IDE1006 // Naming Styles
        {
            var hostBuilder = ConfigureHost(new HostBuilder());
            await hostBuilder.Build().RunAsync();
        }

        internal static IHostBuilder ConfigureHost(IHostBuilder hostBuilder)
        {
            return hostBuilder
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .UseConsoleLifetime()
                .ConfigureServices(services =>
                {
                    services.Configure<ConsoleLifetimeOptions>(options =>
                    {
                        options.SuppressStatusMessages = true;
                    });
                    services.AddHostedService<BattleshipsConsoleGameService>();
                })
                .ConfigureContainer<ContainerBuilder>(builder =>
                {
                    builder.RegisterType<ConsoleProvider>().As<IProvideConsoleStreams>().InstancePerDependency();
                    builder.RegisterType<GameController>().As<IGameController>().SingleInstance();
                    builder.RegisterType<BoardController>().As<IBoardController>().InstancePerDependency();

                    builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                    .Where(t => t.IsInNamespace(
                        string.Join(".", typeof(Program).Namespace, nameof(Handlers), nameof(Handlers.Impl)))
                    ).AsImplementedInterfaces().InstancePerLifetimeScope();

                    builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                    .Where(t => t.IsInNamespace(
                        string.Join(".", typeof(Program).Namespace, nameof(Handlers), nameof(Handlers.ConsoleCommands), nameof(Handlers.ConsoleCommands.Impl)))
                    ).AsImplementedInterfaces().InstancePerLifetimeScope();

                    builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                    .Where(t => t.IsInNamespace(
                        string.Join(".", typeof(Program).Namespace, nameof(Providers), nameof(Providers.Impl)))
                    ).AsImplementedInterfaces().InstancePerLifetimeScope();
                })
                .ConfigureLogging(loggingBuilder =>
                {
                });
        }
    }
}
