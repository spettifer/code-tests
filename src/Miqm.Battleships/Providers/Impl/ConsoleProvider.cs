using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Miqm.Battleships.Providers.Impl
{
    [ExcludeFromCodeCoverage]
    class ConsoleProvider : IProvideConsoleStreams
    {
        public TextReader ConsoleIn => Console.In;

        public TextWriter ConsoleOut => Console.Out;

        public TextWriter ConsoleErr => Console.Error;

        public void Clear()
        {
            Console.Clear();
        }
    }
}
