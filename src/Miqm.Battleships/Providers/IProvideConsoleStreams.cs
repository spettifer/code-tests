using System.IO;

namespace Miqm.Battleships
{
    interface IProvideConsoleStreams
    {
        TextReader ConsoleIn { get; }
        TextWriter ConsoleOut { get; }
        TextWriter ConsoleErr { get; }

        public void Clear();
    }
}
