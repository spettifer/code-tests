namespace Miqm.Battleships.Providers
{
    interface IProvideGameStartCommandText
    {
        public string StartCommand { get; }
    }
}
