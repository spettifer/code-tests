using System.Collections.Generic;

namespace Miqm.Battleships.Providers
{
    interface IProvideCommandHelpText
    {
        IEnumerable<string> HandledCommandsList { get; }
    }
}
