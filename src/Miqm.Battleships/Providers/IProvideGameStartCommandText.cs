namespace Miqm.Battleships.Providers
{
    interface IProvideWelcomeMessage
    {
        public string WelcomeMessage { get; }
    }
}
