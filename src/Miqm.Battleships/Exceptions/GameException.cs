using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Miqm.Battleships.Exceptions
{
    [Serializable]
    [ExcludeFromCodeCoverage]
    class GameException : Exception
    {
        public GameException()
        {
        }

        public GameException(string message) : base(message)
        {
        }

        public GameException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GameException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
